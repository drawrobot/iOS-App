//
//  HostUnreachableViewController.swift
//  SirDrawALot
//
//  Created by Daniel Windhager on 25/06/15.
//  Copyright (c) 2015 Daniel Windhager. All rights reserved.
//

import UIKit

protocol HostUnreachableAlertDelegate {
    func didUpdateHostAndPort()
    func didCancelAlert()
}

class HostUnreachableViewController: UIViewController {

    @IBOutlet var yesButton: UIButton!
    @IBOutlet var noButton: UIButton!
    var delegate: HostUnreachableAlertDelegate?
    var connectButton: UIButton?
    var hostTextField: UITextField?
    var portTextField: UITextField?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.layer.cornerRadius = 4.0
        // Do any additional setup after loading the view.
    }
    
    @IBAction func yesButtonTapped(sender: UIButton) {
        let x = yesButton.frame.origin.x
        let y = yesButton.frame.origin.y
        let width = (noButton.frame.origin.x+noButton.frame.width)-yesButton.frame.origin.x
        
        yesButton.removeFromSuperview()
        noButton.removeFromSuperview()
        hostTextField = UITextField(frame: CGRectMake(x, y, width, 40))
        hostTextField!.placeholder = "Host"
        hostTextField!.borderStyle = UITextBorderStyle.RoundedRect
        hostTextField!.alpha = 0.0
        hostTextField!.textAlignment = NSTextAlignment.Center
        portTextField = UITextField(frame: CGRectMake(x, y+60, width, 40))
        portTextField!.placeholder = "Port"
        portTextField!.alpha = 0.0
        portTextField!.borderStyle = UITextBorderStyle.RoundedRect
        portTextField!.textAlignment = NSTextAlignment.Center
        portTextField!.keyboardType = UIKeyboardType.NumberPad
        portTextField!.text = "1521"
        
        connectButton = UIButton()
        connectButton!.setTitleColor(UIColor(red: 41/255, green: 128/255, blue: 185/255, alpha: 1.0), forState: .Normal)
        connectButton!.setTitle("Connect", forState: UIControlState.Normal)
        connectButton!.sizeToFit()
        connectButton!.frame.origin = CGPointMake((self.view.frame.width-connectButton!.frame.width)/2, portTextField!.frame.origin.y+60)
        connectButton!.alpha = 0.0
        connectButton!.addTarget(self, action: Selector("updateHostAndPort"), forControlEvents: UIControlEvents.TouchUpInside)
        self.view.addSubview(hostTextField!)
        self.view.addSubview(portTextField!)
        self.view.addSubview(connectButton!)
        UIView.animateWithDuration(0.3, animations: { () -> Void in
            self.hostTextField!.alpha = 1.0
            self.portTextField!.alpha = 1.0
            self.connectButton!.alpha = 1.0
            self.view.frame.size = CGSizeMake(self.view.frame.width, self.view.frame.height+100)
        })
    }
    
    @IBAction func noButtonTapped(sender: UIButton) {
        delegate?.didCancelAlert()
    }
    
    func updateHostAndPort() {
        if hostTextField != nil && hostTextField!.text != "" && portTextField != nil && portTextField!.text != "" {
            if let host = hostTextField?.text {
                if let port = Int(portTextField!.text!) {
                    TCPStream.sharedStream().setSharedStream(host, port)
                    delegate?.didUpdateHostAndPort()
                }
            }
        }
    }
}
