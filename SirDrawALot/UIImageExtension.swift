//
//  UIImageExtension.swift
//  SirDrawALot
//
//  Created by Daniel Windhager on 24/02/15.
//  Copyright (c) 2015 Daniel Windhager. All rights reserved.
//

import UIKit

extension UIImage {
    
    /**
    ** Extension method on UIImage to supply an easy way to resize images
    **/
    class func resizeImage(image: UIImage, toSize targetSize: CGSize) -> UIImage {
        let size = image.size
        
        let widthRatio  = targetSize.width  / image.size.width
        let heightRatio = targetSize.height / image.size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSizeMake(size.width * heightRatio, size.height * heightRatio)
        } else {
            newSize = CGSizeMake(size.width * widthRatio,  size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRectMake(0, 0, newSize.width, newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.drawInRect(rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
    
    /**
    ** Returns a normalized representation of the current image
    **/
    func normalizedImage() -> UIImage {
        if (self.imageOrientation == UIImageOrientation.Up)
        {
            return self
        }
        UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale);
        self.drawInRect(CGRectMake(0, 0, self.size.width, self.size.height))
        let normalizedImage: UIImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        return normalizedImage;
    }
    
}