//
//  ViewController.swift
//  SirDrawALot
//
//  Created by Daniel Windhager on 25.11.14.
//  Copyright (c) 2014 Daniel Windhager. All rights reserved.
//

import UIKit

/**
    This class is the main entry point for the Application. The user is asked to login before he is able to continue.
*/
class ViewController: UIViewController, TCPStreamDelegate, UITextFieldDelegate, HostUnreachableAlertDelegate {

    var tcpStream: TCPStream? = nil
    @IBOutlet var username: UITextField!
    @IBOutlet var password: UITextField!
    var unreachableAlert: HostUnreachableViewController?
    var containerView: UIView?
    
    private var indicatorBackgroundView: UIView?
    private var indicatorView: UIActivityIndicatorView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        username.becomeFirstResponder()
    }
    
    func tcpServerAcceptedUser() {
        stopActivityIndicator()
        print("User accepted")
        performSegueWithIdentifier("login", sender: self)
    }
    
    func tcpServerDeclinedUser() {
        stopActivityIndicator()
        print("User declined")
        alert("Error",desc: "User or Password wrong!")
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if username.isEqual(textField) {
            password.becomeFirstResponder()
        } else {
            password.resignFirstResponder()
            tcpStream = TCPStream.sharedStream()
            if tcpStream == nil {
                print("Connection could not be established!")
                print("Is the IP Adress and port set right?")
                return false
            }
            if username.text == "" || password.text == "" {
                alert("Error", desc: "You have to supply a username and password")
                return false
            }
            tcpStream?.delegate = self
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), { () -> Void in
                self.tcpStream!.authenticate(self.username.text!, password: self.password.text!)
            })
            
            // Display load indicator
            startActivityIndicator()
        }
        return true
    }
    
    func hostUnreachable() {
        stopActivityIndicator()
        print("Host unreachable")
        let alertUnreachable = HostUnreachableViewController(nibName: "HostUnreachableViewController", bundle: nil)
        alertUnreachable.delegate = self
        displayHostUnreachable(alertUnreachable)
    }
    
    func didCancelAlert() {
        dismissUnreachableAlert()
    }
    
    func didUpdateHostAndPort() {
        dismissUnreachableAlert()
    }
    
    func dismissUnreachableAlert() {
        username.text = ""
        password.text = ""
        UIView.animateWithDuration(0.3, animations: {
            self.containerView?.alpha = 0.0
            self.unreachableAlert?.view.alpha = 0.0
            }) { (check) in
                self.unreachableAlert?.view.removeFromSuperview()
                self.containerView?.removeFromSuperview()
                self.containerView = nil
                self.unreachableAlert = nil
        }
    }
    
    @IBAction func unwindToLogin(segue: UIStoryboardSegue) {
    }
    
    private func displayHostUnreachable(viewController: HostUnreachableViewController) {
        let x = (self.view.frame.width-viewController.view.frame.width)/2.0
        let y = CGFloat(80)
        containerView = UIView(frame: self.view.frame)
        containerView!.backgroundColor = UIColor.grayColor().colorWithAlphaComponent(0.5)
        viewController.view.frame = CGRectMake(x, y, viewController.view.frame.width, viewController.view.frame.height)
        self.unreachableAlert = viewController
        containerView!.addSubview(viewController.view)
        containerView!.alpha = 0.0
        self.view.addSubview(containerView!)
        UIView.animateWithDuration(0.4, animations: { () -> Void in
            self.containerView!.alpha = 1.0
        })
    }
    
    /**
    ** Displays an alert to the user with given message and description
    ** :param: msg The Message to display
    ** :param: desc The description for the alert
    **/
    func alert(msg: String, desc: String) {
        let alertController = UIAlertController(title: msg, message: desc, preferredStyle: UIAlertControllerStyle.Alert)
        alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
            self.dismissViewControllerAnimated(true, completion: nil)
        }))
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    private func startActivityIndicator() {
        let backgroundView = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
        backgroundView.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.2)
        backgroundView.layer.cornerRadius = 5
        backgroundView.center = view.center
        backgroundView.alpha = 0.0
        
        let indicatorView = UIActivityIndicatorView(activityIndicatorStyle: .WhiteLarge)
        indicatorView.center = CGPoint(x: 50, y: 50)
        indicatorView.startAnimating()
        indicatorView.alpha = 0.0
        
        backgroundView.addSubview(indicatorView)
        view.addSubview(backgroundView)
        
        UIView.animateWithDuration(0.2, animations: {
            backgroundView.alpha = 1
            indicatorView.alpha = 1
        })
        
        self.indicatorBackgroundView = backgroundView
        self.indicatorView = indicatorView
    }
    
    private func stopActivityIndicator() {
        indicatorView?.stopAnimating()
        indicatorView?.removeFromSuperview()
        indicatorView = nil
        
        indicatorBackgroundView?.removeFromSuperview()
        indicatorBackgroundView = nil
    }
}

