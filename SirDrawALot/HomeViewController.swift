//
//  HomeViewController.swift
//  SirDrawALot
//
//  Created by Daniel Windhager on 26.11.14.
//  Copyright (c) 2014 Daniel Windhager. All rights reserved.
//

import UIKit

/**
    This class provides the Home Screen for the app. The user can access all different screens from here.
*/
class HomeViewController: UIViewController, TCPStreamDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    var tcpStream: TCPStream?
    
    @IBOutlet var chooseNewImageButton: UIButton!
    @IBOutlet var editExistingImageButton: UIButton!
    @IBOutlet var showDrawnImagesButton: UIButton!
    private var cameraButton: UIButton?
    private var libraryButton: UIButton?
    private var cancelled = false
    var loadingView: UIView?
    var imagePickerController: UIImagePickerController?
    
    var showDrawnImagesViewController: ShowDrawnImagesViewController?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        chooseNewImageButton.layer.cornerRadius = 8.0
        chooseNewImageButton.clipsToBounds = true
        editExistingImageButton.layer.cornerRadius = 8.0
        showDrawnImagesButton.layer.cornerRadius = 8.0
        tcpStream = TCPStream.sharedStream()
        tcpStream?.delegate = self
    
        self.loadingView = UIView(frame: self.chooseNewImageButton.bounds)
        self.loadingView!.backgroundColor = UIColor.colorWithRGB((142, 68, 173), alpha: 0.5)
        self.loadingView!.frame.size = CGSizeMake(0, 1000)
        self.loadingView!.alpha = 1.0
        self.chooseNewImageButton.addSubview(self.loadingView!)
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            self.imagePickerController = UIImagePickerController()
        }
        
        if view.window == nil {
            UIApplication.sharedApplication().keyWindow?.rootViewController = self
        }
    }
    
    @IBAction func chooseNewImageButtonTapped(sender: UIButton) {
        cameraButton = createButtonForTitle("Camera", forBackgroundColor: UIColor(red: 153/255.0, green: 103/255.0, blue: 179/255.0, alpha: 1.0))
        libraryButton = createButtonForTitle("Library", forBackgroundColor: UIColor(red: 206/255.0, green: 114/255.0, blue: 87/255.0, alpha: 1.0))
        libraryButton!.frame.origin.y = cameraButton!.frame.height+64
        
        self.view.addSubview(cameraButton!)
        self.view.addSubview(libraryButton!)
        
        UIView.animateWithDuration(0.5, animations: { () -> Void in
            self.cameraButton!.alpha = 1.0
            self.libraryButton!.alpha = 1.0
        })
    }
    
    func createButtonForTitle(title: String, forBackgroundColor color: UIColor) -> UIButton {
        let size = (width:UIScreen.mainScreen().bounds.width, height:(self.view.frame.height-64)/2)
        let button = UIButton(frame: CGRectMake(0,64,size.width, size.height))
        button.alpha = 0.0
        button.backgroundColor = color
        button.setTitle(title, forState: .Normal)
        button.titleLabel!.font = UIFont.systemFontOfSize(45.0)
        button.addTarget(self, action: "selectionButtonTapped:", forControlEvents: .TouchUpInside)
        button.tag = 13
        return button
    }
    
    func selectionButtonTapped(sender: UIButton) {
        cameraButton?.removeFromSuperview()
        libraryButton?.removeFromSuperview()
        cameraButton = nil
        libraryButton = nil
        if imagePickerController != nil {
            if sender.titleLabel!.text=="Camera" && UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera) {
                imagePickerController!.sourceType = UIImagePickerControllerSourceType.Camera
                imagePickerController!.showsCameraControls = true
            } else if sender.titleLabel!.text == "Library" {
                imagePickerController!.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
            } else {
                let alertController = UIAlertController(title: "No Camera available", message: "It seems that you have no camera ;)", preferredStyle: UIAlertControllerStyle.Alert)
                alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
                self.presentViewController(alertController, animated: true, completion: nil)
            }
            imagePickerController!.delegate = self
            self.presentViewController(imagePickerController!, animated: true, completion: nil)
        }
    }
    
    /**
    ** This method is called when the user has chosen an image from the library or the camera
    **/
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage!, editingInfo: [NSObject : AnyObject]!) {
        if let x = self.view.viewWithTag(13) {
            x.removeFromSuperview()
        }
        if let x = self.view.viewWithTag(13) {
            x.removeFromSuperview()
        }
        
        self.dismissViewControllerAnimated(true, completion: nil)
        let resizedImage = UIImage.resizeImage(image.normalizedImage(), toSize: CGSizeMake(568, 1136))
        TCPStream.sharedStream().write(image: resizedImage, completionHandler: {
            print("Picked Image!")
            dispatch_async(dispatch_get_main_queue()) {
                self.loadingView!.frame.size = CGSizeMake(0, self.loadingView!.frame.size.height)
                self.loadingView!.alpha = 1.0
            }
        })
    }
    
    
    func tcpStreamSentPercentOfImage(percent: Float) {
        dispatch_async(dispatch_get_main_queue()) {
            if self.view.userInteractionEnabled == true {
                self.view.userInteractionEnabled = false
            }
            if self.loadingView != nil {
                    UIView.animateWithDuration(0.1, animations: { () -> Void in
                        self.loadingView!.frame.size.width = self.chooseNewImageButton.bounds.size.width*CGFloat(percent)
                        print("Size is \(self.loadingView!.frame.size.width)")
                    })
            }
        }
    }
    
    func tcpStreamSentImage() {
        dispatch_async(dispatch_get_main_queue()) {
            self.view.userInteractionEnabled = true
            if self.loadingView != nil {
                    UIView.animateWithDuration(0.2, animations: { () -> Void in
                        self.loadingView!.alpha = 0.0
                    })
            }
        }
    }
    
    func tcpStremaSentIDForTransmittedImage(id: Int) {
        self.performSegueWithIdentifier("editExistingImage", sender: id)
    }
    
    func tcpInputStreamDidClose() {
        cancel()
    }
    func tcpOutputStreamDidClose() {
        cancel()
    }
    
    private func cancel() {
        if !cancelled {
            cancelled = true
            let alertController = UIAlertController(title: "Connection Lost", message: "Going back to Login Screen", preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
                self.dismissViewControllerAnimated(true, completion: {
                    self.performSegueWithIdentifier("unwindToLogin", sender: self)
                })
            }))
            self.presentViewController(alertController, animated: true, completion: nil)
        }
    }
    
    /**
    ** This method will be called before the transition to a different screen begins
    **/
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "editExistingImage" {
            if let controller = segue.destinationViewController as? EditExistingImageViewController {
                controller.imageIdToLoad = sender as? Int
            }
            
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        tcpStream = TCPStream.sharedStream()
        tcpStream!.delegate = self
    }
}

extension UIColor {
    /**
    ** This method converts 3 Int values (0-255) and an alpha value into an UIColor object
    ** :param: rgb Tuple containing 3 values (R, G, B)
    ** :param: alpha Float value indicating the opacity of the picture
    ** :returns: An UIColor object
    **/
    class func colorWithRGB(rgb: (r:Int, g:Int, b:Int), alpha: Float = 1.0) -> UIColor {
        return UIColor(red: CGFloat(rgb.r), green: CGFloat(rgb.g), blue: CGFloat(rgb.b), alpha: CGFloat(alpha))
    }
}