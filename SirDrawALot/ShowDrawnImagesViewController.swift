//
//  ShowDrawnImagesViewController.swift
//  SirDrawALot
//
//  Created by Daniel Windhager on 01.12.14.
//  Copyright (c) 2014 Daniel Windhager. All rights reserved.
//

import UIKit

/**
    This class handles the images which are currently residing on the server. The images, coming from the server, will be displayed in a UICollectionView.
*/
class ShowDrawnImagesViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, TCPStreamDelegate {

    @IBOutlet var collectionView: UICollectionView!
    let queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
    private var cancelled = false
    var images = Array<UIImage>()
    var ids = Array<Int>()
    
    var currentEditingView: UIView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        TCPStream.sharedStream().delegate = self
        TCPStream.sharedStream().getIDsForImages()
    }
    
    override func viewWillAppear(animated: Bool) {
        currentEditingView?.removeFromSuperview()
        super.viewWillAppear(animated)
    }

    // Collection View Functions
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return ids.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("Cell", forIndexPath: indexPath)
        var image: UIImage?
        
        if images.count <= indexPath.row {
            image = UIImage()
        } else {
            image = self.images[indexPath.row]
        }
        let imageView = UIImageView(image: image)
        imageView.contentMode = UIViewContentMode.ScaleToFill
        imageView.clipsToBounds = true
        cell.backgroundView = imageView
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let cell = collectionView.cellForItemAtIndexPath(indexPath)
        if currentEditingView != nil && currentEditingView != cell!.viewWithTag(13) {
            UIView.animateWithDuration(0.2, animations: {
                    self.currentEditingView!.alpha = 0.0
                }, completion: { (check) -> Void in
                    self.currentEditingView!.removeFromSuperview()
                    self.currentEditingView = nil
                    self.collectionView(collectionView, didSelectItemAtIndexPath: indexPath)
            })
        } else if cell!.viewWithTag(13) == nil && currentEditingView == nil {
            let accessoryView = AccessoryCellView(frame: CGRectMake(0,0,cell!.bounds.width, cell!.bounds.height))
            accessoryView.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.0)
            accessoryView.tag = 13
            cell!.addSubview(accessoryView)
            currentEditingView = accessoryView
            
            accessoryView.addAction({
                UIView.animateWithDuration(0.2, animations: {
                    accessoryView.alpha = 0.0
                }, completion: { (check) -> Void in
                    accessoryView.removeFromSuperview()
                    self.currentEditingView = nil
                })
            }, forType: .Default)
            
            accessoryView.addAction({
                self.performSegueWithIdentifier("editExistingImage", sender: cell)
            }, forType: .Edit)
            
            accessoryView.addAction({
                TCPStream.sharedStream().deleteImage("\(self.ids[indexPath.row])")
                self.ids.removeAtIndex(indexPath.row)
                self.images.removeAtIndex(indexPath.row)
                collectionView.reloadData()
            }, forType: .Delete)
            UIView.animateWithDuration(0.2, animations: {
                accessoryView.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.4)
                }) { (check) -> Void in
                    accessoryView.makeVisible()
            }
        }
    }
    
    func collectionView(collectionView: UICollectionView, didEndDisplayingCell cell: UICollectionViewCell, forItemAtIndexPath indexPath: NSIndexPath) {
        if let view = cell.viewWithTag(13) {
            view.removeFromSuperview()
            currentEditingView = nil
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "editExistingImage" {
            if let cell = sender as? UICollectionViewCell {
                if let imageView = cell.backgroundView as? UIImageView {
                    if let image = imageView.image {
                        let dest = segue.destinationViewController as! EditExistingImageViewController
                        dest.imageToEdit = image
                    }
                }
            }
        }
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        let containerSize = collectionView.frame.size
        return CGSizeMake(containerSize.width, containerSize.height/2.3)
    }
    
    // Server Functions
    
    func tcpServerDidSendImageIDs(ids: Array<Int>) {
        self.ids = ids
        
        TCPStream.sharedStream().requestImage("\(ids[0])")
    }
    
    func tcpServerDidSendImage(image: UIImage) {
        
        self.images.append(image)
        if images.count == 1 {
            self.collectionView.reloadData()
        } else {
            self.collectionView.reloadItemsAtIndexPaths([NSIndexPath(forItem: images.count-1, inSection: 0)])
        }
        if self.images.count < self.ids.count {
            TCPStream.sharedStream().requestImage("\(self.ids[self.images.count])")
        }
    }
    
    func tcpInputStreamDidClose() {
        cancel()
    }
    
    func tcpOutputStreamDidClose() {
        cancel()
    }
    
    private func cancel() {
        if !cancelled {
            cancelled = true
            let alertController = UIAlertController(title: "Connection Lost", message: "Going back to Login Screen", preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
                self.dismissViewControllerAnimated(true, completion: {
                    self.performSegueWithIdentifier("unwindToLogin", sender: self)
                })
            }))
            self.presentViewController(alertController, animated: true, completion: nil)
        }
    }
}
