//
//  DrawTextVC.swift
//  SirDrawALot
//
//  Created by Daniel Windhager on 14/02/15.
//  Copyright (c) 2015 Daniel Windhager. All rights reserved.
//

import UIKit


/**
    This class enables the user to tell the server to draw some text.
*/
class DrawTextVC: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var textfield: UITextField!
    var lastValidString = ""
    var textLayer: CATextLayer!
    private var cancelled = false
    override func viewDidLoad() {
        super.viewDidLoad()
        textLayer = CATextLayer()
        let size = view.frame.size
        textLayer.frame = CGRectMake(0, 164, size.width, 50)
        textLayer.foregroundColor = UIColor.blackColor().CGColor
        textLayer.alignmentMode = kCAAlignmentCenter
        textLayer.wrapped = true
        textLayer.font = CGFontCreateWithFontName(UIFont.systemFontOfSize(40).fontName as CFString)
        textLayer.string = "Your text"
        textLayer.contentsScale = UIScreen.mainScreen().scale
        self.view.layer.addSublayer(textLayer)
        textfield.tintColor = UIColor.clearColor()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        textfield.becomeFirstResponder()
    }

    @IBAction func textChanged(sender: UITextField) {
        if textfield.text!.characters.count > 10 {
            textfield.text = lastValidString
        }
        else {
            lastValidString = textfield.text!
            textLayer.string = textfield.text
        }
        
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        displaySendAlert()
        return true
    }
    
    
    /**
    ** This displays an alert, asking the user if he really wants to send the
    ** text to the server
    **/
    func displaySendAlert() {
        let alertController = UIAlertController(title: "Draw Text", message: "Are you sure you want \"\(self.textfield.text!)\" drawn?", preferredStyle: UIAlertControllerStyle.Alert)
        alertController.addAction(UIAlertAction(title: "No", style: .Cancel, handler: nil))
        alertController.addAction(UIAlertAction(title: "Yes", style: .Default, handler: {
            action in
            TCPStream.sharedStream().drawText(self.textfield.text!)
            self.navigationController?.popToRootViewControllerAnimated(true)
        }))
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    
}
