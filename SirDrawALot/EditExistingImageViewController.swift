//
//  EditExistingImageViewController.swift
//  SirDrawALot
//
//  Created by Daniel Windhager on 01.12.14.
//  Copyright (c) 2014 Daniel Windhager. All rights reserved.
//

import UIKit

/**
    This class provides an interface for the user to apply a threshhold filter to the image, and tell the server to draw the image.
*/
class EditExistingImageViewController: UIViewController, TCPStreamDelegate {

    var imageToEdit: UIImage?
    var imageIdToLoad: Int? = nil
    @IBOutlet var imageView: UIImageView!
    var working = false
    private var cancelled = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if imageToEdit != nil {
            imageView.image = imageToEdit!
            print("Image: \(imageToEdit!.size)")
        } else if imageIdToLoad != nil {
            TCPStream.sharedStream().requestImage("\(imageIdToLoad!)", callback: { (image) -> () in
                self.imageView.image = image
                self.imageToEdit = image
            })
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func sliderValueChanged(sender: UISlider) {
        if !working && imageToEdit != nil {
            working = true
            let start = NSDate()
            Threshold.threshholdImage(imageToEdit!, value: sender.value) { (image: UIImage?) in
                self.imageView.image = image
                self.working = false
                
                let end = NSDate()
                print("Time: \((end.timeIntervalSinceDate(start))*1000)ms")
            }
        }
    }
    
    func tcpOutputStreamDidClose() {
        cancel()
    }
    
    func tcpInputStreamDidClose() {
        cancel()
    }
    
    private func cancel() {
        if !cancelled {
            cancelled = true
            let alertController = UIAlertController(title: "Connection Lost", message: "Going back to Login Screen", preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
                self.dismissViewControllerAnimated(true, completion: {
                    self.performSegueWithIdentifier("unwindToLogin", sender: self)
                })
            }))
            self.presentViewController(alertController, animated: true, completion: nil)
        }
    }
    

}
