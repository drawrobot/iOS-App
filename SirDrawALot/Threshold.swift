//
//  Threshold.swift
//  SirDrawALot
//
//  Created by Daniel Windhager on 11.01.15.
//  Copyright (c) 2015 Daniel Windhager. All rights reserved.
//

import UIKit

/**
** This class transforms a given Image by applying a threshhold
**/
class Threshold: NSObject {
    
    private struct __threshold_thread {
        static let queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
    }
    
    /**
    ** Static method to apply a threshhold to an image
    ** :param: image The image to transform
    ** :param: value The float value used for the threshhold
    ** :param: completionHandler A callback which is passed the transformed image upon completion
    **/
    class func threshholdImage(image: UIImage, value: Float, completionHandler:(image: UIImage?)->()) {
        dispatch_async(__threshold_thread.queue) {
            let imageRef = image.CGImage
            
            let height:UInt = UInt(CGImageGetHeight(imageRef))
            let width: UInt = UInt(CGImageGetWidth(imageRef))
            
            let bytesPerPixel:Int = 4
            let bytesPerRow:Int = bytesPerPixel * Int(width)
            let bitsPerComponent: Int = 8
            
            let pixels: UnsafeMutablePointer<UInt32> = UnsafeMutablePointer<UInt32>.alloc(Int(width*height))
            let colorSpace = CGColorSpaceCreateDeviceRGB()
            let bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.PremultipliedLast.rawValue|CGBitmapInfo.ByteOrder32Big.rawValue)
            let context = CGBitmapContextCreate(pixels, Int(width), Int(height), bitsPerComponent, bytesPerRow, colorSpace, bitmapInfo.rawValue)
            CGContextDrawImage(context, CGRectMake(0,0,CGFloat(width),CGFloat(height)), imageRef)
            var currentPixel:UInt32 = UInt32()
            let iheight = Int(height)
            let iwidth = Int(width)
            
            for var i = 0; i < iheight; i++ {
                for var j = 0; j < iwidth; j++ {
                    currentPixel = pixels[i*iwidth + j]
                    let r = Int(currentPixel & 0xFF)
                    let g = Int((currentPixel >> 8) & 0xFF)
                    let b = Int((currentPixel >> 16) & 0xFF)
                    
                    let brightness = Float(3*r + g + 2*b) / 6.0
                    if brightness <= value*255 {
                        pixels[i*iwidth + j] = 4278190080
                    } else {
                        pixels[i*iwidth + j] = 4294967295
                    }
                }
            }
            
            let context_image = CGBitmapContextCreateImage(context)
            let newImage = UIImage(CGImage: context_image!)
            dispatch_async(dispatch_get_main_queue()) {
                return completionHandler(image: newImage)
            }
        }
    }
    
}
