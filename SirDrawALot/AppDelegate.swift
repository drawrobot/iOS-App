//
//  AppDelegate.swift
//  SirDrawALot
//
//  Created by Daniel Windhager on 25.11.14.
//  Copyright (c) 2014 Daniel Windhager. All rights reserved.
//

import UIKit
import Google

/**
    Main entry and management point for the application.
 
 
    CURL - Command:
 
    curl --request POST --url http://gcm-http.googleapis.com/gcm/send --header 'authorization:key=AIzaSyAHJRIkVf9ZH1O6D-vNtvSIwM5PW4g02IY' --header 'content-type: application/json' --data '{"collapse_key" : "score_update", "data": {"badge": "0","content-available": "1", "alert": "Some important message", "sound":""},"registration_ids":["lqBIcKjia-I:APA91bEL4X_CI5bw7lAlCmmebKvdzl4z_7xm2hukWKXfJ60SJdjzo5CenFQoJdMswSqsIy9JA2dnW6dWz5gwC0ddcDfYN4oDClJas4-TNDGemBXgRNj6mIYg84A5o7xCSVwEF8O9BVJl","ef0aE7C9cJw:APA91bHLq1p6YRmS7G_KE1ESW073_qBPusXc4zRgI5n1b4lntgo3xJyi3Bl0lB0Lke1kixcqBxC6voe-d4byZdfsYdDwtF3ICV5CdfD_fp-97IuXi9UqOr_2C-KxVwertu-VgYQ6iJiq"]}'*/
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, GGLInstanceIDDelegate, GCMReceiverDelegate {

    var window: UIWindow?
    var connectedToGCM = false
    var subscribedToTopic = false
    var gcmSenderID: String?
    var registrationToken: String?
    var registrationOptions = [String: AnyObject]()
    
    let registrationKey = "onRegistrationCompleted"
    let messageKey = "onMessageReceived"
    let subscriptionTopic = "/topics/global"

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        
        var configureError:NSError?
        GGLContext.sharedInstance().configureWithError(&configureError)
        assert(configureError == nil, "Error configuring Google services: \(configureError)")
        gcmSenderID = GGLContext.sharedInstance().configuration.gcmSenderID

        let settings: UIUserNotificationSettings =
        UIUserNotificationSettings(forTypes: [.Alert, .Badge, .Sound], categories: nil)
        application.registerUserNotificationSettings(settings)
        application.registerForRemoteNotifications()

    
        let gcmConfig = GCMConfig.defaultConfig()
        gcmConfig.receiverDelegate = self
        GCMService.sharedInstance().startWithConfig(gcmConfig)
        
        application.cancelAllLocalNotifications()

        
        return true
    }
    
    
    func applicationDidBecomeActive( application: UIApplication) {
        GCMService.sharedInstance().connectWithHandler({
            (NSError error) -> Void in
            if error != nil {
                print("Could not connect to GCM: \(error.localizedDescription)")
            } else {
                self.connectedToGCM = true
                print("Connected to GCM")
                // [START_EXCLUDE]
                //self.subscribeToTopic()
                // [END_EXCLUDE]
            }
        })
    }
    
    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        let instanceIDConfig = GGLInstanceIDConfig.defaultConfig()
        instanceIDConfig.delegate = self

        GGLInstanceID.sharedInstance().startWithConfig(instanceIDConfig)
        registrationOptions = [kGGLInstanceIDRegisterAPNSOption:deviceToken,
            kGGLInstanceIDAPNSServerTypeSandboxOption:true]
        GGLInstanceID.sharedInstance().tokenWithAuthorizedEntity(gcmSenderID,
            scope: kGGLInstanceIDScopeGCM, options: registrationOptions, handler: registrationHandler)
    }
    
    func application( application: UIApplication, didFailToRegisterForRemoteNotificationsWithError
        error: NSError ) {
            print("Registration for remote notification failed with error: \(error.localizedDescription)")
            let userInfo = ["error": error.localizedDescription]
            NSNotificationCenter.defaultCenter().postNotificationName(
                registrationKey, object: nil, userInfo: userInfo)
    }

    func application( application: UIApplication,
        didReceiveRemoteNotification userInfo: [NSObject : AnyObject]) {
            print("Notification received: \(userInfo)")
            GCMService.sharedInstance().appDidReceiveMessage(userInfo);
            
            presentNotificationInApplication(application, withTitle: "SirDrawALot", andBody: userInfo["text"] as! String)
            
            NSNotificationCenter.defaultCenter().postNotificationName(messageKey, object: nil,
                userInfo: userInfo)
    }
    
    
    func application( application: UIApplication,
        didReceiveRemoteNotification userInfo: [NSObject : AnyObject],
        fetchCompletionHandler handler: (UIBackgroundFetchResult) -> Void) {
            print("Notification received: \(userInfo)")
            
            presentNotificationInApplication(application, withTitle: "SirDrawALot", andBody: userInfo["text"] as! String)
            
            GCMService.sharedInstance().appDidReceiveMessage(userInfo);

            NSNotificationCenter.defaultCenter().postNotificationName(messageKey, object: nil,
                userInfo: userInfo)
            handler(UIBackgroundFetchResult.NoData);
    }
    
    func registrationHandler(registrationToken: String!, error: NSError!) {
        if (registrationToken != nil) {
            self.registrationToken = registrationToken
            print("Registration Token: \(registrationToken)")
            
            //self.subscribeToTopic()
            let userInfo = ["registrationToken": registrationToken]
            NSNotificationCenter.defaultCenter().postNotificationName(
                self.registrationKey, object: nil, userInfo: userInfo)
        } else {
            print("Registration to GCM failed with error: \(error.localizedDescription)")
            let userInfo = ["error": error.localizedDescription]
            NSNotificationCenter.defaultCenter().postNotificationName(
                self.registrationKey, object: nil, userInfo: userInfo)
        }
    }
    
    func onTokenRefresh() {
        print("The GCM registration token needs to be changed.")
        GGLInstanceID.sharedInstance().tokenWithAuthorizedEntity(gcmSenderID,
            scope: kGGLInstanceIDScopeGCM, options: registrationOptions, handler: registrationHandler)
    }
    
    func willSendDataMessageWithID(messageID: String!, error: NSError!) {
        if (error != nil) {
            // Failed to send the message.
        } else {
            // Will send message, you can save the messageID to track the message
        }
    }
    
    func didSendDataMessageWithID(messageID: String!) {
        // Did successfully send message identified by messageID
    }
    // [END upstream_callbacks]
    
    func didDeleteMessagesOnServer() {
        // Some messages sent to this device were deleted on the GCM server before reception, likely
        // because the TTL expired. The client should notify the app server of this, so that the app
        // server can resend those messages.
    }
    
    func applicationWillTerminate(application: UIApplication) {
        // The TCPStream is disconnected if the app is closed
        TCPStream.sharedStream().disconnect()
    }
    
    func applicationDidEnterBackground(application: UIApplication) {
        TCPStream.sharedStream().disconnect()
        
        GCMService.sharedInstance().disconnect()
//        // [START_EXCLUDE]
        self.connectedToGCM = false
    }
    
    
    private func presentNotificationInApplication(app: UIApplication, withTitle title: String, andBody body: String) {
        let alert = UIAlertController(title: title, message: body, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "Yeah!", style: UIAlertActionStyle.Default, handler: nil))
        app.keyWindow?.rootViewController?.presentViewController(alert, animated: true, completion: nil)
    }
    
}

