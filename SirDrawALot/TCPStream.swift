//
//  TCPStream.swift
//  TCPClient
//
//  Created by Daniel Windhager on 29.10.14.
//  Copyright (c) 2014 Daniel Windhager. All rights reserved.
//

import UIKit


private var _tcpStream = /*TCPStream(IPAdress: "172.20.10.6", port:1521)*/TCPStream(IPAdress: "sirdrawalot.tk", port: 1521)

/**
    This protocol includes several events for users of the TCPStream class, to notify them of different events occuring on the server, the connection, etc..


    - tcpServerDidSendImage 
        This event occurs only after an image was requested.
    - tcpInputStreamDidOpen 
        Sent when the input stream was opened successfully.
    - tcpOutputStreamDidOpen    
        Sent when the output stream was opened successfully.
    - tcpInputStreamDidClose    
        Sent when the input stream was closed successfully.
    - tcpOutputStreamDidClose   
        Sent when the output stream was closed successfully.
    - tcpRequestedImageNotFoundOnServer 
        Sent when the image with the requested ID was not found on the server.
    - tcpServerAcceptedUser 
        Sent when the sent user credentials were accepted by the server.
    - tcpServerDeclinedUser 
        Sent when the sent user credentials were declined by the server.
    - tcpServerDidSendImageIDs  
        This event only occurs after all image IDs, currently residing on the server, were requested.
    - tcpStreamSentPercentOfImage   
        This is a continous event which is sent mutliples times during the transmission of an image.
    - tcpStreamSentImage    
        Sent when the server has sent an image.
*/
@objc
protocol TCPStreamDelegate {
    optional func tcpServerDidSendImage(image: UIImage)
    optional func tcpInputStreamDidOpen()
    optional func tcpOutputStreamDidOpen()
    optional func tcpInputStreamDidClose()
    optional func tcpOutputStreamDidClose()
    optional func tcpRequestedImageNotFoundOnServer()
    optional func tcpServerAcceptedUser()
    optional func tcpServerDeclinedUser()
    optional func tcpServerDidSendImageIDs(ids: Array<Int>)
    optional func tcpStreamSentPercentOfImage(percent: Float)
    optional func tcpStreamSentImage()
    optional func tcpStremaSentIDForTransmittedImage(id: Int)
    optional func hostUnreachable()
}

/**
    This class provides an interface to the server using TCP. 
*/
class TCPStream: NSObject, NSStreamDelegate {
    var inputStream: NSInputStream?
    var outputStream: NSOutputStream?
    private var ipAdress: String?
    private var port: Int?
    private var authenticating: Bool = false
    private var imageSentCallback: ((image: UIImage)->())? = nil
    private var connected = false
    var delegate: TCPStreamDelegate?
    
    private var spaceAvailable = false
    override var description: String {
        get {
            return "Host: \(self.ipAdress!), Port: \(port!)"
        }
    }
    let queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
    
    
    class func sharedStream() -> TCPStream {
        return _tcpStream
    }
    
    //
    // Init Functions
    //
    
    private init(IPAdress: String, port: Int) {
        self.ipAdress = IPAdress
        self.port = port
        super.init()
        initNetworkCommunication()
    }
    
    
    //
    // Public Functions
    //
    
    func setSharedStream(host: String,_ port: Int) {
        _tcpStream = TCPStream(IPAdress: host, port: port)
    }
    
    func reset() {
        self.inputStream = nil
        self.outputStream = nil
        self.connected = false
        self.authenticating = false
    }
    
    func stream(aStream: NSStream, handleEvent eventCode: NSStreamEvent) {
        switch(eventCode)
        {
        case NSStreamEvent.OpenCompleted:
            connected = true
            print("\(nameForStream(aStream)) opened")
            break;
            
        case NSStreamEvent.HasBytesAvailable:
            read()
            break;
            
        case NSStreamEvent.ErrorOccurred:
            connected = false
            inputStream = nil
            outputStream = nil
            print("Can not connect to the host!");
            break;
            
        case NSStreamEvent.EndEncountered:
            connected = false
            inputStream = nil
            outputStream = nil
            print("\(nameForStream(aStream)) did close connection!")
            break;
        case NSStreamEvent.HasSpaceAvailable:
            print("Space available")
            spaceAvailable = true
        default:
            print("Default Event");
        }
    }
    
    /**
        Writes simple text to the open Stream
    
        :param: Text The text to be written
        :param: Flush Optional parameter to indicate whether to flush immeadietly or not. Default value is true
     
        :returns: Nothing
    */
    func write(text: String, flush: Bool = true) {
        dispatch_async(queue) {
            while !self.spaceAvailable {}
            let x = flush ? "\r\n" : ""
            let data = "\(text)\(x)".dataUsingEncoding(NSASCIIStringEncoding)!
            self.outputStream?.write(UnsafePointer<UInt8>(data.bytes), maxLength:data.length)
            self.spaceAvailable = false
        }
    }
    
    /**
        Writes a custome image to the TCP Stream
    
        :param: image The image to be written
    
        :returns: No return value
    */
    func write(image image: UIImage, completionHandler:(() -> ()) = {}) {

        dispatch_async(queue)
        {
            let base64 = UIImagePNGRepresentation(image)
            let stringRep = "01\(base64!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions.Encoding64CharacterLineLength))" // Get base64 String
            let data: NSData = stringRep.dataUsingEncoding(NSASCIIStringEncoding)! // Convert base64 to NSData
    
        
            var bytes = UnsafePointer<UInt8>(data.bytes)
            var bytesLeftToWrite: NSInteger = data.length
            let bytesTotal = bytesLeftToWrite
        
            while bytesLeftToWrite > 0 {
                while !self.spaceAvailable {}
                if self.inputStream == nil || self.outputStream == nil {
                    self.delegate?.tcpInputStreamDidClose?()
                    self.delegate?.tcpOutputStreamDidClose?()
                    return
                }
                let bytesWritten = self.outputStream!.write(bytes, maxLength: bytesLeftToWrite)
                if bytesWritten == -1 {
                    print("ERROR occured during writing.")
                    break // Some error occurred ...
                }
                
                bytesLeftToWrite -= bytesWritten
                bytes += bytesWritten // advance pointer
                let part = Float(bytesTotal-bytesLeftToWrite)/Float(bytesTotal)
                self.delegate?.tcpStreamSentPercentOfImage?(Float(part))
                //println("\(bytesLeftToWrite) Bytes left to write.")
            }
            if self.inputStream == nil || self.outputStream == nil {
                self.delegate?.tcpInputStreamDidClose?()
                self.delegate?.tcpOutputStreamDidClose?()
                return
            }
            self.delegate?.tcpStreamSentPercentOfImage?(Float(1.000001))
            let flushData = "\n\r".dataUsingEncoding(NSASCIIStringEncoding)!
            self.outputStream!.write(UnsafePointer<UInt8>(flushData.bytes), maxLength: flushData.length)
            self.delegate?.tcpStreamSentImage?()
            self.spaceAvailable = false
            completionHandler()
        }
        
    }
    
    
    /**
        Requests an Image from the Server via the open TCP Stream
    
        :param: ID ID of the Image on the server
        :param: BlackAndWhiteFactor An integer value to indicate the threshold value
        :param: Size The size of the requested Image
    
        :returns: Nothing
    */
    func requestImage(id: String, blackAndWhiteFactor: Int = -1,size: (width: Int, height: Int) = (-1,-1), callback: (image:UIImage)->() = {x in}) {
        if size.width != -1 && size.height != -1 {
            write("05\(id):\(size.width):\(size.height)")
        } else if (blackAndWhiteFactor != -1){
            write("04\(id):\(blackAndWhiteFactor)")
        } else {
            write("05\(id)")
        }
        self.imageSentCallback = callback
    }

    
    /**
        Log in to the server as User
    
        :param: User The Username
        :param: The password
    
        :returns: Nothing
    */
    func authenticate(user: String, password: String) {        
        dispatch_sync(queue) {
            self.write("00\(user):\(password)")
        }
    }
    
    
    /**
        Returns IDs of all Images available on the server for the current User
    
        :param: Nothing
    
        :returns: Nothing
    */
    func getIDsForImages() {
        write("03")
    }
    
    

    /**
        Sends a disconnect statement to the server
    
        :param: Nothing
    
        :returns: Nothing
    */
    func disconnect() {
        write("99")
        outputStream?.close()
        inputStream?.close()
        delegate?.tcpInputStreamDidClose?()
        delegate?.tcpOutputStreamDidClose?()
    }
    
    
    func deleteImage(id: String) {
        write("06\(id)")
    }
    
    func drawText(text: String) {
        write("07\(text)", flush: true)
    }
    
    //
    // Private Functions
    //
    
    private func initNetworkCommunication() {
        NSStream.getStreamsToHostWithName(ipAdress!, port: port!, inputStream: &inputStream, outputStream: &outputStream)
        inputStream?.delegate = self
        outputStream?.delegate = self
        inputStream?.scheduleInRunLoop(NSRunLoop.mainRunLoop(), forMode: NSDefaultRunLoopMode)
        outputStream?.scheduleInRunLoop(NSRunLoop.mainRunLoop(), forMode: NSDefaultRunLoopMode)
        inputStream?.open()
        outputStream?.open()
        delegate?.tcpInputStreamDidOpen?()
        delegate?.tcpOutputStreamDidOpen?()
        let time = dispatch_time(DISPATCH_TIME_NOW, Int64(5.0 * Double(NSEC_PER_SEC)))
        dispatch_after(time, dispatch_get_main_queue()) { () -> Void in
            print("IN")
            if !self.connected {
                self.delegate?.hostUnreachable?()
                self.inputStream = nil
                self.outputStream = nil
            }
        }
    }
    
    private func read() {
        var totalString: String = ""
        var sentString = ""
        repeat {
            if inputStream == nil || outputStream == nil {
                delegate?.tcpInputStreamDidClose?()
                delegate?.tcpOutputStreamDidClose?()
                return
            }
            var buffer = [UInt8](count: 1024, repeatedValue: 0)
            self.inputStream?.read(&buffer, maxLength: 1024)
            let up = UnsafePointer<UInt8>(buffer)
            if up.memory == 0 {
                print("Server seems to have shut down!")
                self.delegate?.tcpInputStreamDidClose?()
                self.delegate?.tcpOutputStreamDidClose?()
                return
            } else {
                sentString = NSString(data: NSData(bytes: up, length: 1024), encoding: NSASCIIStringEncoding)! as String
                totalString += sentString
            }
        
        } while(sentString.rangeOfString("\n") == nil)
        let prefix = totalString.substringToIndex(totalString.startIndex.advancedBy(2))
        totalString = totalString.substringFromIndex(totalString.startIndex.advancedBy(2))
    
        switch(prefix)
        {
        case "00": self.handleAuthentication(totalString)
            break;
        case "01": self.handleImageID(totalString)
            break;
        case "02": self.handleBase64EncodedImage(totalString)
            break;
        case "03": self.handleAvailableImageIDs(totalString)
            break;
        default:
        break;
        }
    }
    
    private func nameForStream(aStream: NSStream) -> String {
        if aStream.isKindOfClass(NSInputStream)
        {
            return "InputStream"
        }
        return "OutputStream"
    }
    
    private func handleAuthentication(string: String) {
        if string.rangeOfString("false") != nil {
            delegate?.tcpServerDeclinedUser?()
        } else if string.rangeOfString("true") != nil {
            delegate?.tcpServerAcceptedUser?()
            
            sendRegistrationToken()
        }
    }
    
    private func sendRegistrationToken() {
        if let registrationToken = (UIApplication.sharedApplication().delegate as? AppDelegate)?.registrationToken {
            write(registrationToken)
        }
    }
    
    private func handleImageID(string: String) {
        print("Received ID (\(string)) for uploaded Image.")
        let totalString = string.stringByReplacingOccurrencesOfString("\r", withString: "")
        let x = totalString.stringByReplacingOccurrencesOfString("\n", withString: "")
        let id = NSString(string: x).integerValue
        delegate?.tcpStremaSentIDForTransmittedImage?(id)
    }
    
    private func handleBase64EncodedImage(string: String) {
        if let xdata = NSData(base64EncodedString: string, options: NSDataBase64DecodingOptions.IgnoreUnknownCharacters) {
            if let ximage = UIImage(data: xdata) {
                delegate?.tcpServerDidSendImage?(ximage)
                if let callback = self.imageSentCallback {
                    callback(image: ximage)
                }
                return
            }
        }
        delegate?.tcpRequestedImageNotFoundOnServer?()
        print("Image doesn't exist on Server.")
    }
    
    private func handleAvailableImageIDs(string: String) {
        if string.rangeOfString(";") != nil {
            var totalString = string.stringByReplacingOccurrencesOfString("\r", withString: "")
            totalString = totalString.stringByReplacingOccurrencesOfString("\n", withString: "")
            let elements = totalString.componentsSeparatedByString(";") as [String]
            let array = elements.map(
                {
                    if let x = Int($0)
                    {
                        return x
                    }
                    return -1
            }).filter
                {
                    element in
                    if element != -1 {
                        return true
                    }
                    return false
                } as Array<Int>
            delegate?.tcpServerDidSendImageIDs?(array)
            print("Sent image IDs: \(array)")
        }
    }

}
