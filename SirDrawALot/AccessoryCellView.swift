//
//  AccessoryCellView.swift
//  SirDrawALot
//
//  Created by Daniel Windhager on 11.01.15.
//  Copyright (c) 2015 Daniel Windhager. All rights reserved.
//

import UIKit
import Darwin

/**
    This enum is used to determine which the two separate buttons as well as a default value.
    The following states are available:

    - Edit
    - Delete 
    - Default
*/
enum AccessoryType: String {
    case Edit = "Edit"
    case Delete = "Delete"
    case Default = "Default"
}

/**
    This class which derives from UIView is used as an extra layer in the ShowDrawnImagesViewController when the user taps on one of the images. It then displays two options for the user to choose:

        - Delete image
        - Process Image
*/
class AccessoryCellView: UIView {

    var actions: [AccessoryType : (() -> ())]? = nil

    var editLayer: CALayer!
    var deleteLayer: CALayer!
    
    var tapGestureRecognizer: UITapGestureRecognizer!
    
    let BUTTON_SIZE:CGFloat = 80

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {        
        super.init(frame: frame)
        
        self.editLayer = CALayer()
        self.deleteLayer = CALayer()
        self.tapGestureRecognizer = UITapGestureRecognizer(target: self, action: "tapRecognized:")
        self.tapGestureRecognizer.numberOfTapsRequired = 1
        self.tapGestureRecognizer.numberOfTouchesRequired = 1
        
        self.addGestureRecognizer(tapGestureRecognizer)
        setupLayers()
    }

    func addAction(action: ()->(),forType type: AccessoryType) {
        if actions == nil {
            actions = Dictionary<AccessoryType,(() -> ())>()
        }
        actions![type] = action
    }
    
    
    private func setupLayers() {
        
        let size = self.frame.size
        
        self.editLayer.bounds.size = CGSizeMake(BUTTON_SIZE, BUTTON_SIZE)
        self.editLayer.position = CGPointMake(size.width/4, size.height/2)
        //editLayer.backgroundColor = UIColor.whiteColor().CGColor
        
        self.deleteLayer.bounds.size = CGSizeMake(BUTTON_SIZE, BUTTON_SIZE)
        self.deleteLayer.position = CGPointMake((size.width/4)*3, size.height/2)
        //deleteLayer.backgroundColor = UIColor.whiteColor().CGColor
        
        self.layer.addSublayer(editLayer)
        self.layer.addSublayer(deleteLayer)
        
    }
    
    func tapRecognized(sender: UITapGestureRecognizer) {
        let location = sender.locationInView(self)
        let types = [AccessoryType.Edit, AccessoryType.Delete]
        let radius = BUTTON_SIZE/2
        for type in types {
            let layer = type == .Edit ? editLayer : deleteLayer
            let xDist = location.x - layer.position.x
            let yDist = location.y - layer.position.y
            
            let distance = sqrt((xDist*xDist)+(yDist*yDist))
            
            // Button Tapped
            if distance <= radius {
                if let action = actions?[type] {
                    action()
                    return
                }
            }
        }
        if let action = actions?[.Default] {
            action()
        }
        
        
    }
    
    func makeVisible() {
        drawButton(self.editLayer, type: .Edit)
        drawButton(self.deleteLayer, type: .Delete)
    }
    
    private func drawButton(layer: CALayer, type: AccessoryType) {
        let accessPath = UIBezierPath()
        let clockwise = type == .Delete
        let rect = layer.bounds
        let radius = layer.bounds.width/2.0
        
        var startAngle = CGFloat(90.01 * M_PI/180)
        var endAngle = CGFloat(90 * M_PI/180)
        
        if !clockwise {
            let swap = startAngle
            startAngle = endAngle
            endAngle = swap
        }
        
        accessPath.addArcWithCenter(CGPointMake(CGRectGetMidX(rect), CGRectGetMidY(rect)), radius: radius, startAngle: startAngle, endAngle: endAngle, clockwise: clockwise)
        
        if type == .Delete {
            let center = CGPointMake(CGRectGetMidX(rect), CGRectGetMidY(rect))
            let start = CGPointMake(center.x-radius/2, center.y)
            let end = CGPointMake(center.x+radius/2, center.y)
            
            for var i = 0; i < 2; i++ {
                let path1 = UIBezierPath()
                path1.moveToPoint(start)
                path1.addLineToPoint(end)
                
                let layer1 = CAShapeLayer()
                layer1.strokeColor = UIColor.whiteColor().CGColor
                layer1.lineCap = kCALineCapRound
                layer1.lineWidth = 5.0
                layer1.path = path1.CGPath
                layer1.bounds = path1.bounds
                layer1.position = CGPointMake(BUTTON_SIZE/2, BUTTON_SIZE/2)
                layer.addSublayer(layer1)
                
                let reveal = CABasicAnimation()
                reveal.keyPath = "strokeEnd"
                //reveal.duration = 0.5
                reveal.fromValue = 0.0
                reveal.toValue = 1.0
                
                let cross = CABasicAnimation()
                cross.keyPath = "transform.rotation.z"
                cross.fromValue = NSNumber(double: 0.0)
                cross.toValue = NSNumber(double: Double((i == 0 ? -45 : 45.0) * CGFloat(M_PI)/180.0))
                cross.fillMode = kCAFillModeForwards
                cross.removedOnCompletion = false
                
                layer1.transform = CATransform3DMakeRotation((i == 0 ? -45 : 45.0) * CGFloat(M_PI)/180.0, 0, 0, 1)
                
                let group = CAAnimationGroup()
                group.animations = [reveal, cross]
                group.duration = 0.7
                
                layer1.addAnimation(group, forKey: nil)
            }
        } else {
            let center = CGPointMake(CGRectGetMidX(rect), CGRectGetMidY(rect))
            let lineLength = rect.width/2
            let start = CGPointMake(lineLength/2, center.y)
            let end = CGPointMake(lineLength+start.x, center.y)
            
            let mainPath = UIBezierPath()
            mainPath.moveToPoint(start)
            mainPath.addLineToPoint(end)
            
            let mainLayer = CAShapeLayer()
            mainLayer.path = mainPath.CGPath
            mainLayer.lineCap = kCALineCapRound
            mainLayer.lineWidth = 5.0
            mainLayer.fillColor = UIColor.clearColor().CGColor
            mainLayer.strokeColor = UIColor.whiteColor().CGColor
            
            for var i = 0; i < 2; i++ {
                let path1 = UIBezierPath()
                path1.moveToPoint(center)
                path1.addLineToPoint(CGPointMake(center.x+start.x, center.y))
                
                let layer1 = CAShapeLayer()
                layer1.path = path1.CGPath
                layer1.fillColor = UIColor.clearColor().CGColor
                layer1.lineCap = kCALineCapRound
                layer1.bounds = path1.bounds
                layer1.position = CGPointMake(end.x, end.y)
                layer1.anchorPoint = CGPointMake(1.0,0.5)
                layer1.lineWidth = 5.0
                layer1.strokeColor = UIColor.whiteColor().CGColor
                
                
                let tiltAnimation = CABasicAnimation()
                tiltAnimation.duration = 0.6
                tiltAnimation.keyPath = "transform.rotation.z"
                tiltAnimation.fromValue = NSNumber(double: 0.0)
                tiltAnimation.toValue = NSNumber(double: Double((i == 0 ? -30 : 30) * CGFloat(M_PI)/180.0))
                
                layer1.transform = CATransform3DMakeRotation((i == 0 ? -30 : 30) * CGFloat(M_PI)/180.0, 0, 0, 1)
                
                layer1.addAnimation(tiltAnimation, forKey: nil)
                
                mainLayer.addSublayer(layer1)
            }
            
            
            layer.addSublayer(mainLayer)
        }
        
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = accessPath.CGPath
        shapeLayer.strokeColor = UIColor.whiteColor().CGColor
        shapeLayer.lineCap = kCALineCapRound
        shapeLayer.lineWidth = 5.0
        shapeLayer.fillColor = UIColor.clearColor().CGColor
        layer.addSublayer(shapeLayer)
        
        let animation = CABasicAnimation(keyPath: "strokeEnd")
        animation.duration = 0.8
        animation.fromValue = 0.0
        animation.toValue = 1.0
        
        shapeLayer.addAnimation(animation, forKey: nil)
    }
    
    
}
