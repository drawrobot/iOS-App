//
//  ImageManager.swift
//  SirDrawALot
//
//  Created by Daniel Windhager on 18.01.15.
//  Copyright (c) 2015 Daniel Windhager. All rights reserved.
//

import UIKit

class ImageManager: NSObject {
    
    private func getCachesURL() -> NSURL {
        let fileManager = NSFileManager()
        let urls = fileManager.URLsForDirectory(NSSearchPathDirectory.CachesDirectory, inDomains: NSSearchPathDomainMask.UserDomainMask) as [NSURL]
        return urls[0]
    }
    
    func idsForCurrentlySavedImagesAtCachesDirectory() -> [String] {
        return getIdsForCurrentlyStoredImagesWithExtensions().map({$0.removeExtension()})
    }
    
    private func getIdsForCurrentlyStoredImagesWithExtensions() -> [String] {
        let fileManager = NSFileManager()
        var contents = fileManager.contentsOfDirectoryAtPath(getCachesURL().path!, error: nil) as [String]
        contents = contents.filter({ element in
            if element[element.startIndex] == "." {
                return false
            }
            if element.endsWithEitherOneOf([".jpeg",".jpg", ".png"]) {
                return true
            }
            return false
        })
        return contents
    }
    
    func getStoredImageForId(id: String) -> UIImage? {
        for storedId in self.getIdsForCurrentlyStoredImagesWithExtensions() {
            if storedId.removeExtension() == id {
                let fullPath = getCachesURL().URLByAppendingPathComponent(storedId)
                if let image = UIImage(named: fullPath.path!) {
                    return image
                }
            }
        }
        return nil
    }
    
    func storeImage(image: UIImage, forId id: String) {
        let fullPath = getCachesURL().URLByAppendingPathComponent(id+".jpg")
        UIImageJPEGRepresentation(image, 0.7).writeToURL(fullPath, atomically: true)
    }
}

extension String {
    func endsWithEitherOneOf(ends: [String]) -> Bool {
        for ending in ends {
            let range = Range(start: advance(self.endIndex, -NSString(string: ending).length), end: self.endIndex)
            let substring = self.substringWithRange(range)
            if substring.lowercaseString == ending {
                return true
            }
        }
        return false
    }
    
    func removeExtension() -> String {
        var bool = false
        var current = ""
        for char in reverse(self) {
            if bool {
                current.insert(char, atIndex: current.startIndex)
            }
            if char == "." {
                bool = true
            }
        }
        return current
    }
}
